# Symfony Demo Application With PosgreSQL

This repository is a fork from Symfony, implemented to use PostgreSQL instead of SQLite for demo purposes, with the help of [pgloader](https://pgloader.io) to migrate e

It contains a `data.dump` file to test backup restauration with `pg_restore` command on Clever Cloud.

The "Symfony Demo Application" is a reference application created to show how
to develop applications following the [Symfony Best Practices][1].

You can also learn about these practices in [the official Symfony Book][5].

## Requirements

- PHP 8.1.0 or higher;
- PostgreSQL (tested with version 14 and 15)
- and the [usual Symfony application requirements][2].

## Installation

Clone the code repository and install its dependencies

```shell
git clone <remote> symfony-pg
cd symfony-pg
composer install
```

## Deploy on Clever Cloud

Deploy this application on Clever Cloud by following these steps.

### 1. Create the app on Clever Cloud

Add the following environment variables:

```shell
APP_ENV="prod"
APP_SECRET="2ca64f8d83b9e89f5f19d672841d6bb8"
CC_PHP_VERSION="8"
CC_WEBROOT="/public"
```

(`APP_ENV` is given for demo purposes, if you need a unique one, check **Go Further** section)
Don't forget to click on **UPDATE CHANGES**!

Link a PosgreSQL add-on

### 2. Push to Clever Cloud

You are ready to deploy! Push your code to Clever Cloud with the remote information from the console.

### 3. Feed the database

While your app is deploying, import the dummy data in the database.

Run the following command at the root of this repo:

```pgsql
pg_restore -h POSTGRESQL_ADDON_HOST -p POSTGRESQL_ADDON_PORT -U POSTGRESQL_ADDON_USER -d  POSTGRESQL_ADDON_DB --format=c data/data.dump
```

Copy-paste the value of `POSTGRESQL_ADDON_PASSWORD` in your terminal when asked for it.

After the restauration, you should see the demo-app with posts and be able to connect with the demo credentials on the admin page.

## Go further

You can change data by running a PostgreSQL server on your machine and adding this variable to `.env`:

`POSTGRESQL_ADDON_URI="postgresql://localhost:5432/<your-db-name>?user=postgres&password"`

- Change `your-db-name` with whatever you want to name your local db
- `5432` is the default port for PostgreSQL, chage it if you use another one
- `postgres` is the default username as, change it if you set up another one

Then use doctrine to create the database with `php bin/console doctrine:database:create`

Then restore the sample data file if you want it in your local app:

```pgsql
pg_restore -h localhost -p 5432 -U postgres -d <your-db-name> --format=c data/data.dump
```

### Serve the app

[Download Symfony CLI][4] and run this command:

```shell
symfony serve
```

Then access the application in your browser at the given URL (<https://localhost:8000> by default).

## Generate a New Secret

Use `php bin/console secrets:generate-keys` to generate a new secret key.

[1]: https://symfony.com/doc/current/best_practices.html
[2]: https://symfony.com/doc/current/setup.html#technical-requirements
[3]: https://symfony.com/doc/current/setup/web_server_configuration.html
[4]: https://symfony.com/download
[5]: https://symfony.com/book
[6]: https://getcomposer.org/
